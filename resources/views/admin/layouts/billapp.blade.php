<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@isset($pageTitle) {{$pageTitle}} @endisset Invoice</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet"
          href="{{asset('almasaeed2010/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet"
          href="{{asset('almasaeed2010/adminlte/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('almasaeed2010/adminlte/bower_components/Ionicons/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('almasaeed2010/adminlte/dist/css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{asset('almasaeed2010/adminlte/dist/css/skins/_all-skins.min.css')}}">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <script src="{{asset('realrashid/sweet-alert/src/js/sweetalert.all.js')}}"></script>


    @yield('header')
</head>

<body class="hold-transition skin-blue sidebar-mini">

@yield('content')


<script src="{{asset('almasaeed2010/adminlte/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('almasaeed2010/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('almasaeed2010/adminlte/dist/js/adminlte.min.js')}}"></script>


@yield('script')


</body>
</html>
