@extends('admin.layouts.app')
@section('header')

<link rel="stylesheet"
      href="{{asset('almasaeed2010/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

        <ol class="breadcrumb">
            <li><a href="{{URL::to('/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{URL::to($routeTo)}}">{{$pageTitle}}</a></li>
            <li class="active"><a href="">{{ucfirst($data->name)}}</a></li>

        </ol>
        <h2 class="box-title">
            {{$pageTitle}}
        </h2>
    </section>

    <!-- Main content -->


    <section class="content">

        @include('errors.errors')
        <!-- Info boxes -->
        <div class="box">
            <div class="box-header">

            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <!-- ./col -->
                    <div class="col-md-12">
                        <div class="box box-solid">
                            <a href="{{URL::to($routeTo)}}" class="btn  btn-default">
                                <i class="fa fa-step-backward"></i> Back
                            </a>

                            <div class="box-header with-border">
                                <i class="fa fa-text-width"></i>

                                <h3 class="box-title">Match Details</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <dl class="dl-horizontal">
                                    <dt>
                                    <h3>Match</h3></dt>
                                    <dd><h3>{{ucwords($data->gamedetails->name)}}</h3></dd>
                                    <dt>Image</dt>
                                    <dd><img class="img img-thumbnail" src="{{asset('photos/games/'.$data->gamedetails->image)}}"></dd>
                                    <dt>Kick-off</dt>
                                    <dd>@isset($data->gamedetails->date) {{ucwords($data->gamedetails->date).' '.
                                        $data->gamedetails->kick_off}} @endisset
                                    </dd>
                                    <dt>Competition</dt>
                                    <dd>{{ucfirst($data->competition)}}</dd>
                                    <dt>Stadium</dt>
                                    <dd>@isset($data->gamedetails->stadium) {{ucwords($data->gamedetails->stadium)}} @endisset
                                    </dd>
                                    <dt>Link
                                    </dt>
                                    <dd>{{$data->link}}</dd>
                                </dl>
                            </div>

                            <div class="box-body">
                                <dl class="dl-horizontal">
                                    <dt>Live Links</dt>

                                    @foreach($data->links as $key=>$d)
                                    <dd><a href="{{$d->links}}">Link {{$key=$key+1}}: {{$d->links}}</a></dd>

                                    @endforeach


                                </dl>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- ./col -->
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.row -->


        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>


@endsection


