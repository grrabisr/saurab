<?php

namespace App\Http\Controllers;


use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Goutte\Client;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use PHPUnit\Runner\Exception;
use App\Services\ConstantMessageService;
use Intervention\Image\Facades\Image as Image;

use Illuminate\Support\Facades\Storage; //Laravel Filesystem


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
        // $this->client = $client;
        $client = new Client();
        $i = 0;
//  Hackery to allow HTTPS
        $guzzleclient = new \GuzzleHttp\Client([
            'timeout' => 60,
            'verify' => false,
        ]);

//  Hackery to allow HTTPS
        $this->client = $client->setClient($guzzleclient);
        $this->matchLists = array();
        $this->gameUrl = array();
        $this->matchDetails = array();
        $this->pageTitle = "Games";
        $this->redirectUrl = env('ROUTE_PREFIX_ADMIN') . '/dashboard';
        $this->viewUrl = env('ROUTE_PREFIX_ADMIN') . "/dashboard";
        $this->routeTo = "/" . env('ROUTE_PREFIX_ADMIN') . "/dashboard/";
        $this->imagename = '';


    }

    function get_string_between($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {

        //all games link and pages of first page
        $mainPage = $this->client->request('GET', 'https://www.ronaldo7.net/video/watch-football-live.html');
        $mainPage->filter('#Quadro .style117')->filter('a')->each(function ($node) {
            // echo 'Game->' . $node->text() . '<br>'; //name of games
            $this->matchLists[$node->text()] = $node->attr('href');
        });

        //end

        // print_r($this->matchLists); die();

        foreach ($this->matchLists as $key => $matches) {

            //link of specific match crawler
            $matchLists = $this->client->request('GET', $matches);
            $i = 0;
            $matchLists->filter('#Quadro .style208')->filter('a')->each(function ($node) use ($key, &$i) {
                //echo 'href->' . $node->attr('href') . '<br>'; //list of livestreaming link
                $this->gameUrl[$key][$i++] = $node->attr('href');


            });
            //end link list of specific mOatch
        }
        // print_r($this->gameUrl); die();

        /*$i = 0;
        foreach ($this->matchLists as $key => $matches) {

            $matchDetailsSingle = $this->client->request('GET', $matches);
            $matchDetailsSingle->filter('.style263')->each(function ($node) use ($key, &$i) {
                $match = $this->get_string_between($node->text(), 'Match:', 'Date:');
                $this->matchDetails[$i]['match'] = trim($match);
                $date = $this->get_string_between($node->text(), $match . 'Date:', 'Stadium:');
                $this->matchDetails[$i]['date'] = trim($date);
                $stadium = $this->get_string_between($node->text(), $date . 'Stadium:', 'Competition:');
                $this->matchDetails[$i]['stadium'] = trim($stadium);
                $competition = $this->get_string_between($node->text(), $stadium . 'Competition:', 'Kickoff time:');
                $this->matchDetails[$i]['competition'] = trim($competition);
                $kickoff = substr($node->text(), strpos($node->text(), "Kickoff time:") + 13);
                $this->matchDetails[$i]['kickoff'] = trim($kickoff);


                $i++;
            });
           // print_r($this->matchDetails);
        }
        print_r($this->matchDetails);*/


        //print_r($this->matchLists); die();
        //print_r($this->matchLists); die();
        //gets matche details
        //img[src="https://www.ronaldo7.net/minfo.png"]

        foreach ($this->matchLists as $key => $matches) {

            $matchDetailsSingle = $this->client->request('GET', $matches);
//          / echo $matches;
            // $matchDetailsSingle->filter('.style9 img[src="https://www.ronaldo7.net/minfo.png"]')->each(function ($node) {
            $matchDetailsSingle->filter('#Quadro p')->eq(5)->each(function ($node) {
                // $matchDetailsSingle->filter('.style263, .style273, .style216')->each(function ($node) {
                /*
                                print_r($node->html());
                                print_r($node->attr('src')); die();*/
                $matchDetails = array();
                $match = $this->get_string_between($node->text(), 'Match:', 'Date:');
                $matchDetails['match'] = trim($match);
                $date = $this->get_string_between($node->text(), $match . 'Date:', 'Stadium:');
                $matchDetails['date'] = trim($date);
                $stadium = $this->get_string_between($node->text(), $date . 'Stadium:', 'Competition:');
                $matchDetails['stadium'] = trim($stadium);
                $competition = $this->get_string_between($node->text(), $stadium . 'Competition:', 'Kickoff time:');
                $matchDetails['competition'] = trim($competition);
                $kickoff = substr($node->text(), strpos($node->text(), "Kickoff time:") + 13);
                $matchDetails['kickoff'] = trim($kickoff);
                print_r($matchDetails);


            });
        }

        //get matches details end


    }

    function save_image($local_file, $remote_file)
    {


        $ch = curl_init();
        $fp = fopen($local_file, 'w+');
        $ch = curl_init($remote_file);
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);
    }

    public function master()
    {


        try {
            //all games link and pages of first page
            $mainPage = $this->client->request('GET', 'https://www.ronaldo7.net/video/watch-football-live.html');
            $mainPage->filter('#Quadro .style117')->filter('a')->each(function ($node) {
                // echo 'Game->' . $node->text() . '<br>'; //name of games
                $this->matchLists[$node->text()] = $node->attr('href');
            });
            //end
//match Details
            foreach ($this->matchLists as $key => $matches) {

                $matchDetailsSingle = $this->client->request('GET', $matches);

                $matchDetailsSingle->filter('#Quadro p')->eq(5)->each(function ($node) use (&$matches) {
                    $matchDetails = array();
                    $textfetched = $node->text();

                    //   print_r($node->text()); //die();
                    $match = $this->get_string_between($node->text(), 'Match:', 'Date:');
                    $matchDetails['match'] = strtolower(trim($match));

                    $date = $this->get_string_between($node->text(), $match . 'Date:', 'Stadium:');
                    $matchDetails['date'] = strtolower(trim($date));
                    $date = strtotime($matchDetails['date']);
                    $matchDetails['date'] = date('Y-m-d', $date);

                    $stadium = $this->get_string_between($node->text(), 'Stadium:', 'Competition');
                    $matchDetails['stadium'] = strtolower(trim($stadium));
                    $competition = $this->get_string_between($node->text(), $stadium . 'Competition:', 'Kickoff time:');
                    //$matchDetails['competition'] = trim($competition);
                    $kickoff = substr($node->text(), strpos($node->text(), "Kickoff time:") + 13);
                    $matchDetails['kick_off'] = strtolower(trim($kickoff));
                    // print_r($matchDetails);
                    //array to insert in games table
                    $match = array();
                    $match['name'] = strtolower($matchDetails['match']);
                    $match['status'] = 'active';
                    $match['link'] = $matches;
                    $match['competition'] = strtolower(trim($competition));
                    $matchDetailsSingleForImage = $this->client->request('GET', $matches);

                    $matchDetailsSingleForImage->filter('#Quadro p')->eq(0)->filter('img')->each(function ($node) {

                        $path = $node->attr('src');
                        $this->imagename = 'livefreekick.com-' . basename($path);
                        $pathFolder1 = public_path() . '/photos/games';
                        $pathFolder2 = public_path() . '/photos/games/thumbnails';
                        File::isDirectory($pathFolder1) or File::makeDirectory($pathFolder1, 0777, true, true);
                        File::isDirectory($pathFolder2) or File::makeDirectory($pathFolder2, 0777, true, true);

                        $thumbnailpath = public_path('photos/games/thumbnails/' . $this->imagename);

                        $this->save_image(public_path('photos/games/' . $this->imagename), $path);
                        $this->save_image($thumbnailpath, $path);

                        $img = Image::make($thumbnailpath)->resize(100, 100, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                        $img->save($thumbnailpath);

                        /*  Image::make($path)->save(public_path('photos/games/' . $this->imagename));
                          Image::make($path)->save(public_path('photos/games/thumbnails/' . $this->imagename));
                          $thumbnailpath = public_path('photos/games/thumbnails/' . $this->imagename);
                          $img = Image::make($thumbnailpath)->resize(100, 100, function ($constraint) {
                              $constraint->aspectRatio();
                          });
                          $img->save($thumbnailpath);*/
                    });
                    $matchDetails['image'] = $this->imagename;


                    if (!empty($match['name']) && !empty($match['competition'])) {
                        $ifgamesexist = Games::where('name', $matchDetails['match'])->where('competition', $match['competition'])->first(); //check if clubs exist in games table
                        //end
                        if ($ifgamesexist) {
                            $ifgamesexist = $ifgamesexist->toArray();
                        }

                        if (isset($ifgamesexist['id'])) { //check if exists and update else create new

                            if (!$ifgamesexist) {
                                return Redirect::to('/form')
                                    ->withErrors('Not Found Game id')
                                    ->withInput();
                            }
                            try {


                                $responseupdate = Games::where('id', $ifgamesexist)->update($match);
                                $gameid = Games::where('name', $match['name'])->where('competition', $match['competition'])->first()->toArray()['id'];


                                $resgamedetails = GameDetails::where('game_id', $gameid)->update($matchDetails);
                                $gamelinkreponse = Gamelinks::where('game_id', $gameid)->delete();

                                //print_r($this->matchLists); die();
                                // foreach ($this->matchLists as $key => $matches) {
                                //link of specific match crawler
                                $matchLists = $this->client->request('GET', $matches);

                                $i = 0;
                                $matchLists->filter('#Quadro .style208')->filter('a')->each(function ($node) use (&$i, &$gameid, &$match) {
                                    //echo 'href->' . $node->attr('href') . '<br>'; //list of livestreaming link
                                    if ($node->attr('href')) {
                                        // $this->gameUrl[$key][$i++] = $node->attr('href');
                                        // echo $key.'<br>';

                                        $gameurls = array();
                                        $gameurls['links'] = $node->attr('href');
                                        $gameurls['status'] = 'active';
                                        $gameurls['game_id'] = $gameid;
                                        // print_r($gameurls); die();
                                        $resosegamelinkinsert = Gamelinks::create($gameurls);

                                    }
                                });
                                //end link list of specific mOatch
                            } catch (Exception $e) {
                                return redirect($this->redirectUrl)->withErrors(['alert-danger' => $e->getMessage()]);
                            }


                        } else {

                            try {

                                $gamesresponse = Games::create($match);
                                $matchDetails['game_id'] = $gamesresponse->id;
                                $matchDetails['image'] = $this->imagename;
                                GameDetails::create($matchDetails);

                                $matchLists = $this->client->request('GET', $matches);

                                $i = 0;
                                $matchLists->filter('#Quadro .style208')->filter('a')->each(function ($node) use (&$i, &$gameid, &$matchDetails) {
                                    //echo 'href->' . $node->attr('href') . '<br>'; //list of livestreaming link
                                    if ($node->attr('href')) {
                                        // $this->gameUrl[$key][$i++] = $node->attr('href');
                                        // echo $key.'<br>';

                                        $gameurls = array();
                                        $gameurls['links'] = $node->attr('href');
                                        $gameurls['status'] = 'active';
                                        $gameurls['game_id'] = $matchDetails['game_id'];
                                        // print_r($gameurls); die();
                                        $resosegamelinkinsert = Gamelinks::create($gameurls);

                                    }
                                });
                                //end link list of specific mOatch
                                echo 'created';
                            } catch (Exception  $e) {
                                return redirect($this->redirectUrl)->withErrors(['alert-danger' => $e->getMessage()]);

                            }
                        }
                    }
                });
            }
            return redirect($this->redirectUrl)->withErrors(['alert-success' => ConstantMessageService::UPDATE]);

        } catch (\Exception $e) {
            return redirect($this->redirectUrl)->withErrors(['alert-danger' => $e->getMessage()]);


        }


    }

    public function apidata()
    {

    }

}
