@extends('admin.layouts.app')
@section('header')


@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="{{URL::to('/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active"><a href="{{URL::to($routeTo)}}">{{$pageTitle}}</a></li>
            </ol>
            <h2 class="box-title">
                {{$pageTitle}}
            </h2>
        </section>

        <!-- Main content -->


        <section class="content">

        @include('errors.errors')
        <!-- Info boxes -->
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">


                    <div class="box box-info">

                        <div class="box-body">
                            <form action="{{URL::to('admin/settings/store')}}" method="POST" enctype="multipart/form-data">
                                {!! csrf_field() !!}

                                <div class="row">
                                    <div class="col-xs-6">
                                        <label for="exampleInputEmail1">Name</label>
                                        <input type="text" value="{{$data['name']}}" class="form-control"  name="name"  placeholder="name">
                                    </div>
                                    <div class="col-xs-6">
                                        <label for="exampleInputEmail1">Short Name</label>



                                        <input type="text" value="{{$data['short_name']}}" class="form-control" name="short_name"  placeholder="short_name">
                                    </div>
                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Slogan</label>

                                        <input type="text" value="{{$data['slogan']}}" class="form-control"  name="slogan"  placeholder="slogan">
                                    </div>
                                    <div class="col-xs-6">
                                        <label for="exampleInputEmail1">Address</label>

                                        <input type="text" value="{{$data['address']}}" class="form-control" name="address"  placeholder="address">
                                    </div>

                                    <div class="col-xs-6">
                                        <label for="exampleInputEmail1">Email</label>
                                        <input type="email" value="{{$data['email']}}" class="form-control" name="email"  placeholder="email">
                                    </div>

                                    <div class="col-xs-6">
                                        <label for="exampleInputEmail1">Mobile</label>
                                        <input type="text" value="{{$data['mobile']}}" class="form-control" name="mobile"  placeholder="Mobile">
                                    </div>

                                    <div class="col-xs-6">
                                        <label for="exampleInputEmail1">Phone</label>
                                        <input type="text" value="{{$data['phone']}}" class="form-control" name="phone"  placeholder="phone">
                                    </div>

                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Short Description</label>
                                        <textarea id="" name="short_description" class="form-control"   rows="10" cols="80">
                                            {{$data['short_description']}}
                                    </textarea>
                                    </div>

                                    <div class="col-xs-12">
                                        <label for="exampleInputEmail1">Long Description</label>
                                        <textarea id="editor2" name="long_description"  rows="10" cols="80">
                                           {{$data['long_description']}}
                                    </textarea>
                                    </div>

                                    <div class="col-xs-12">

                                        <label for="exampleInputFile">Image</label>
                                        <div class="widget-user-image">
                                            <img class="img-thumbnail" style="height: 100px; width: auto;" src="{{asset('photos/company/'.$data['image'])}}" alt="User Avatar">
                                        </div>

                                        <input type="file" name="image"  value="" id="image">


                                    </div>
                                </div>
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.row -->


            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>




@endsection

@section('script')
    <script src="{{asset('almasaeed2010/adminlte/bower_components/ckeditor/ckeditor.js')}}"></script>

    <script>

        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor2')
            //bootstrap WYSIHTML5 - text editor
        })
    </script>


@endsection
