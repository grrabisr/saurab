<?php
//frontend
Route::get('/', function () {
    return 'Hello World';
});

Route::get('/home', 'FrontendController@setHomepage')->name('homepage');


//end frontend

Auth::routes();



//Route::get('admin/dashboard', 'Admin\DashboardController@index');





//adminpanel

Route::group(['prefix' => 'admin','namespace' => 'Admin'], function () {
    Route::get('/dashboard', 'DashboardController@index');

    //categories
    Route::get('/categories', 'CategoryController@index');
    Route::get('/categories/create', 'CategoryController@create');
    Route::post('/categories/store', 'CategoryController@store');
    Route::post('/categories/update/{id}', 'CategoryController@update');
    Route::get('/categories/{id}', 'CategoryController@destroy');
    Route::get('/categories/edit/{id}', 'CategoryController@edit');


    /*settings*/
    Route::get('/settings', 'SettingController@index');
    Route::post('/settings/store', 'SettingController@store');


    /*users*/
    Route::get('/users', 'UserController@index');
    Route::get('/users/create', 'UserController@create');
    Route::post('/users/store', 'UserController@store');
    Route::post('/users/update/{id}', 'UserController@update');
    Route::get('/users/{id}', 'UserController@destroy');
    Route::get('/users/edit/{id}', 'UserController@edit');

    /*items*/
    Route::get('/items', 'ItemController@index');
    Route::get('/items/create', 'ItemController@create');
    Route::post('/items/store', 'ItemController@store');
    Route::post('/items/update/{id}', 'ItemController@update');
    Route::get('/items/{id}', 'ItemController@destroy');
    Route::get('/items/edit/{id}', 'ItemController@edit');

    /*table*/
    Route::get('/tables', 'TableController@index');
    Route::get('/tables/create', 'TableController@create');
    Route::post('/tables/store', 'TableController@store');
    Route::post('/tables/update/{id}', 'TableController@update');
    Route::get('/tables/{id}', 'TableController@destroy');
    Route::get('/tables/edit/{id}', 'TableController@edit');


    /*order*/
    Route::get('/orders', 'OrderController@index');
    Route::get('/orders/create', 'OrderController@create');
    Route::post('/orders/store', 'OrderController@store');
    Route::post('/orders/update/{id}', 'OrderController@update');
    Route::get('/orders/{id}', 'OrderController@destroy');
    Route::get('/orders/edit/{id}', 'OrderController@edit');
    Route::get('/orders/bill/{id}', 'OrderController@bill');
    Route::get('/orders/delete-item/{orderid}/{itemid}', 'OrderController@deleteItem');





});


Route::get('/', 'FrontendController@setHomepage')->name('homepage');
Route::post('register-user', 'FrontendController@registeruser')->name('registeruser');
Route::get('user-items/{phone}', 'FrontendController@items')->name('frontenditems');
Route::get('order-item/{itemid}/{ordercode}/{quantity}', 'FrontendController@bookitems')->name('bookitems');
Route::get('/details/{id}', 'FrontendController@details');



