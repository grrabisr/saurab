@extends('admin.layouts.app')
@section('header')

<link rel="stylesheet"
      href="{{asset('almasaeed2010/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

        <ol class="breadcrumb">
            <li><a href="{{URL::to('/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{URL::to($routeTo)}}">{{$pageTitle}}</a></li>
        </ol>
        <h2 class="box-title">
            {{$pageTitle}}
        </h2>
    </section>

    <!-- Main content -->


    <section class="content">

        @include('errors.errors')
        <!-- Info boxes -->
        <div class="box">
            <div class="box-header">
                <a href="{{URL::to($routeTo).'?mode=all'}}" class="btn btn-sm btn-success"
                ><i class="glyphicon glyphicon-eue"></i>Fetch All Games</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.row -->


        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>




@endsection

@section('script')




@endsection