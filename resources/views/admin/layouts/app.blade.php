<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@isset($pageTitle) {{$pageTitle}} @endisset</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet"
          href="{{asset('almasaeed2010/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet"
          href="{{asset('almasaeed2010/adminlte/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('almasaeed2010/adminlte/bower_components/Ionicons/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('almasaeed2010/adminlte/dist/css/AdminLTE.min.css')}}">
    <link rel="stylesheet" href="{{asset('almasaeed2010/adminlte/dist/css/skins/_all-skins.min.css')}}">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <script src="{{asset('realrashid/sweet-alert/src/js/sweetalert.all.js')}}"></script>


    @yield('header')
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">
@if($siteinfo)
        <!-- Logo -->
        <a href="index2.html" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>{{$siteinfo->short_name}}</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>{{$siteinfo->name}}</b></span>
        </a>
@endif
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            {{--<img src="" class="user-image" alt="User Image">--}}
                            <span class="hidden-xs">{{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                {{--<img src="" class="img-circle" alt="User Image">--}}
                                <p>
                                    {{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}
                                    <small>{{{ isset(Auth::user()->created_at) ? Auth::user()->created_at : Auth::user()->created_at }}}</small>
                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                {{--<div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                                </div>--}}
                                <div class="pull-right">
                                    <form action="{{URL::to('logout')}}" method="POST">
                                        {!! csrf_field() !!}

                                        <button class="btn btn-default btn-flat">Log out</button>
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->

                </ul>
            </div>

        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">MAIN NAVIGATION</li>
                <li><a href="{{URL::to('admin/dashboard')}}"><i class="fa fa-circle-o text-red"></i>
                        <span>Dashboard</span></a></li>
                <li><a href="{{URL::to('admin/orders')}}"><i class="fa fa-circle-o text-red"></i>
                        <span>Orders</span></a></li>
                <li><a href="{{URL::to('admin/categories')}}"><i class="fa fa-circle-o text-red"></i>
                        <span>Categories</span></a></li>
                <li><a href="{{URL::to('admin/items')}}"><i class="fa fa-circle-o text-red"></i>
                        <span>Items</span></a></li>
                <li><a href="{{URL::to('admin/tables')}}"><i class="fa fa-circle-o text-red"></i>
                        <span>Tables</span></a></li>
                <li><a href="{{URL::to('admin/users')}}"><i class="fa fa-circle-o text-red"></i>
                        <span>Users</span></a></li>
                <li><a href="{{URL::to('admin/settings')}}"><i class="fa fa-circle-o text-red"></i>
                        <span>Settings</span></a></li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
@yield('content')

<!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.4.0
        </div>
        <strong>Copyright &copy; 2019 <a href="">Saurabh Raj Joshi</a>.</strong> All rights
        reserved.
    </footer>


</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{asset('almasaeed2010/adminlte/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('almasaeed2010/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('almasaeed2010/adminlte/dist/js/adminlte.min.js')}}"></script>


@yield('script')
<div class="modal" id="confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Confirm Delete</h4>
            </div>
            <div class="modal-body">
                <strong>Are you sure, you want to delete?</strong>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i
                            class="glyph-icon icon-close"></i> Cancel
                </button>
                <button type="button" class="btn btn-sm btn-danger" id="delete-btn"><i
                            class="glyph-icon icon-trash"></i> Delete
                </button>

            </div>
        </div>
    </div>
</div>


</body>
</html>
