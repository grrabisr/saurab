<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Items extends Authenticatable
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'price', 'category_id','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        '_token', 'remember_token',
    ];

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id', 'id');
    }
}
