<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        if ( $request->is('*/store/*')) {
            return [
                'name' => 'required|min:8',
                'email' => 'email|required',
                'password' => 'min:8|required',
            ];
        }
        if ( $request->is('*/update/*')) {
            return [
                'name' => 'required|min:8',
                'email' => 'email|required',
            ];
        }
        return [
            //
        ];
    }
}
