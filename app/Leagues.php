<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leagues extends Model
{
    protected $table = 'leagues';

    protected $fillable = [
        'id', 'area_id', 'area_name','name','code','emblemUrl','plan','currentid','currentstartDate','currentendDate','currentcurrentMatchday','currentwinner','numberOfAvailableSeasons','lastUpdated','status','created_at','updated_at'
    ];
}
