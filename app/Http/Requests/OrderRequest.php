<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        if ( $request->is('*/store')) {
            return [
                'code' => 'required',
                'order_by' => 'required',
                'order_by_number' => 'required|min:10',
                'table_id' => 'required',
                'status' => 'required',
            ];
        }
        if ( $request->is('*/update/*')) {
            return [
                'code' => 'required',
                'order_by' => 'required',
                'order_by_number' => 'required|min:10',
                'table_id' => 'required',
                'status' => 'required',
            ];
        }
        return [
            //
        ];
    }
}
