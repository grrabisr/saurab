<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Category;
use App\OrderItem;
use App\Orders;
use App\Tables;
use App\Items;

class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->pageTitle = "Languages";
        $this->redirectUrl = '/dashboard';
        $this->viewUrl =  "/admin/";
        $this->table = new Tables();
        $this->order = new Orders();
        $this->category = new Category();
        $this->orderitem = new OrderItem();
                $this->item = new Items ();

    }

    public function index()
    {
        $data=array();
        $data['remaining_order']= $this->order->where('status','processed')->get()->count();
        $data['cancelled']= $this->order->where('status','cancelled')->get()->count();
                $data['completed']= $this->order->where('status','completed')->get()->count();
                $orderid=$this->order->where('status','completed')->pluck('id')->toArray();



 $data['items'] = $this->orderitem->whereIn('order_id',$orderid)->with('item:*')->get();
          
            $sum=0;
            foreach ($data['items'] as $key => $d) {

                $itemsum=$d->quantity*$d->price;
                $sum=$sum+$itemsum;

            }
            $data['sum']=$sum;
        return view($this->viewUrl . '.dashboard',$data);

    }
}
