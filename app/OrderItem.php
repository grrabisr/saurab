<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class OrderItem extends Authenticatable
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'orders_items';

    protected $fillable = [
        'order_id', 'item_id','quantity','price'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        '_token', 'remember_token',
    ];

    public function item()
    {

        return $this->hasOne('App\Items', 'id', 'item_id');

    }




}
