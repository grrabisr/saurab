@extends('admin.layouts.app')
@section('header')

    <link rel="stylesheet"
          href="{{asset('almasaeed2010/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="{{URL::to('/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active"><a href="{{URL::to($routeTo)}}">{{$pageTitle}}</a></li>
            </ol>
            <h2 class="box-title">
                Edit {{$pageTitle}}
            </h2>
        </section>

        <!-- Main content -->


        <section class="content">

        @include('errors.errors')
        <!-- Info boxes -->
            <div class="box">
                <div class="box-header">

                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <div class="box ">
                        <div class="box-header with-border">
                            <label>Code: {{$data->code}}</label><br>
                            <label>Customer Name: {{$data->order_by}}</label><br>
                            <label>Contact Number of customer: {{$data->order_by_number}}</label>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form class="form-horizontal" action="{{URL::to($routeTo).'/update/'.$data->id}}" method="post">
                            <input name="_token" type="hidden" value="{{csrf_token()}}">
                            <div class="box-body">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Status</label>
                                    <div class="col-sm-10">
                                        <select name="status" class="form-control">
                                            <option value="">Select Category</option>
                                            <option @if($data->status=='processed') selected
                                                    @endif @if($ifcompleted) disabled @endif value="processed">
                                                Processed
                                            </option>
                                            <option @if($data->status=='completed') selected @endif value="completed">
                                                Completed
                                            </option>
                                            <option @if($data->status=='cancelled') selected @endif value="cancelled"
                                                    @if($ifprocessed || $ifcompleted) disabled="" @endif>Cancelled
                                            </option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <a href="{{URL::to($routeTo)}}" class="btn btn-default">Cancel</a>
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>


                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <div class="box">
                <div class="box-header">
                    <h3>Items</h3>
                </div>
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered thead-dark table-striped">
                        <thead>
                        <tr>
                            <th>S.N.</th>
                            <th>Item Name</th>
                            <th>Price(Rs)</th>
                            <th>Quantity</th>
                            <th>Total(Rs)</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($items)
                            @foreach($items as $key=>$d)
                                <tr id="data_{{$d->id}}">
                                    <td>{{$key=$key+1}}</td>
                                    <td>{{$d->item->name}}</td>
                                    <td>{{$d->price}}</td>
                                    <td>{{$d->quantity}}</td>
                                    <td>{{$d->quantity*$d->price}}</td>

                                    <td>
                                        @if($data->status=='completed')
                                            Cannot Be Delete | Order Completed..
                                        @else
                                            <a href="{{URL::to($routeTo).'/delete-item/'.$d->order_id.'/'.$d->item->id}}"
                                               class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-trash"></i>Delete</a>
                                        @endif

                                    </td>
                                </tr>
                            @endforeach

                        @else
                            <tr id="data_">
                                <td colspan="3"> No records found.</td>
                            </tr>
                        @endif



                        </tfoot>

                        <a style="margin-bottom: 20px;" target="_blank" href="{{URL::to($routeTo).'/bill/'.$data->id}}" class="btn btn-sm btn-primary"
                        ><i class="glyphicon glyphicon-print"></i>Print</a>

                    </table>

                </div>
                <label class="" style="margin-left: 20px;">Grand Total: {{$sum}} </label>

            </div>



        </section>
        <!-- /.content -->
    </div>




@endsection

@section('script')

    <script src="{{asset('almasaeed2010/adminlte/bower_components/datatables.net/js/jquery.dataTables.js')}}"></script>
    <link rel="stylesheet"
          href="{{asset('almasaeed2010/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

    <script src="{{asset('almasaeed2010/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    <script>

        $(document).ready(function () {
            $('#example1').DataTable();
        });

        $(document).on('click', '#deleteRecords', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "get",
                        url: "{{URL::to($routeTo).'/'}}" + id,
                        success: function (data, response) {
                            console.log(response);
                            if (data!=0) {
                                swal('Successfully Deleted.');
                                $('#data_' + id).hide();

                            }
                            else {
                                swal('Error!! Could not delete.');

                            }

                        }

                    });
                }
            })


        });

    </script>


@endsection
