<?php

namespace App\Http\Controllers\Admin;


use App\Category;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\ItemRequest;
use App\Items;
use App\OrderItem;
use App\Orders;
use App\Tables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ConstantMessageService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;


class OrderController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->pageTitle = "Orders";
        $this->redirectUrl = env('ROUTE_PREFIX_ADMIN') . '/orders/';
        $this->viewUrl = env('ROUTE_PREFIX_ADMIN') . "/orders";
        $this->routeTo = "/" . env('ROUTE_PREFIX_ADMIN') . "/orders/";
        $this->model = new Orders();
        $this->table = new Tables();
        $this->orderItem = new OrderItem();

    }

    public function index()
    {
        try {
            $data['pageTitle'] = $this->pageTitle;
            $data['routeTo'] = $this->routeTo;
            $data['data'] = $this->model->with('table:*')->orderBy('created_at', 'desc')->get();
            return view($this->viewUrl . '.index', $data);
        } catch (\Exception $e) {
            $data['data'] = null;
            return view($this->viewUrl . '.index', $data)->withErrors(['alert-danger' => ConstantMessageService::READ_ERROR]);
        }
    }

    public function create()
    {
        $data['pageTitle'] = $this->pageTitle;
        $data['routeTo'] = $this->routeTo;
        $data['category'] = $this->category->where('status', true)->get();
        return view($this->viewUrl . '.create', $data);

    }

    public function store(ItemRequest $request)
    {
        $data['pageTitle'] = $this->pageTitle;
        $data['routeTo'] = $this->routeTo;
        try {
            $insertData = $request->except('_token');
            $result = $this->model->create($insertData);
            if ($result) {
                return redirect($this->routeTo)->withErrors(['alert-success' => ConstantMessageService::SUCCESS]);
            } else {
                return redirect($this->routeTo)->withErrors(['alert-danger' => ConstantMessageService::ADD_FAIL]);
            }
        } catch (\Exception $e) {
            return redirect($this->routeTo)->withErrors(['alert-danger' => ConstantMessageService::ADD_FAIL]);
        }
    }

    public function edit($id)
    {
        try {
            $ifExist = $this->model->where('id', $id)->get()->count();
            if (!$ifExist) {
                return redirect($this->routeTo)->withErrors(['alert-danger' => ConstantMessageService::READ_ERROR]);
            }
            $checkproccessed = $this->model->where('id', $id)->where('status', 'processed')->get()->count();
            if ($checkproccessed) {
                $data['ifprocessed'] = $this->orderItem->where('order_id', $id)->get()->count();
            } else {
                $data['ifprocessed'] = 0;
            }

            $checkCompleted = $this->model->where('id', $id)->where('status', 'completed')->get()->count();
            if ($checkCompleted) {
                $data['ifcompleted'] = 1;
            } else {
                $data['ifcompleted'] = 0;
            }
            $data['items'] = $this->orderItem->where('order_id', $id)->with('item:*')->get();
            $data['pageTitle'] = $this->pageTitle;
            $data['routeTo'] = $this->routeTo;
            $data['data'] = $this->model->where('id', $id)->first();
            $sum=0;
            foreach ($data['items'] as $key => $d) {

                $itemsum=$d->quantity*$d->price;
                $sum=$sum+$itemsum;

            }
            $data['sum']=$sum;
            return view($this->viewUrl . '.edit', $data);
        } catch (\Exception $e) {
            return redirect($this->routeTo)->withErrors(['alert-danger' => ConstantMessageService::READ_ERROR]);
        }
    }
    public function bill($id)
    {
        try {
            $ifExist = $this->model->where('id', $id)->get()->count();
            if (!$ifExist) {
                return redirect($this->routeTo)->withErrors(['alert-danger' => ConstantMessageService::READ_ERROR]);
            }
            $checkproccessed = $this->model->where('id', $id)->where('status', 'processed')->get()->count();
            if ($checkproccessed) {
                $data['ifprocessed'] = $this->orderItem->where('order_id', $id)->get()->count();
            } else {
                $data['ifprocessed'] = 0;
            }

            $checkCompleted = $this->model->where('id', $id)->where('status', 'completed')->get()->count();
            if ($checkCompleted) {
                $data['ifcompleted'] = 1;
            } else {
                $data['ifcompleted'] = 0;
            }
            $data['items'] = $this->orderItem->where('order_id', $id)->with('item:*')->get();
            $data['pageTitle'] = $this->pageTitle;
            $data['routeTo'] = $this->routeTo;
            $data['data'] = $this->model->where('id', $id)->first();
            $sum=0;
            foreach ($data['items'] as $key => $d) {

                $itemsum=$d->quantity*$d->price;
                $sum=$sum+$itemsum;

            }
            $data['sum']=$sum;
            return view($this->viewUrl . '.bill', $data);
        } catch (\Exception $e) {
            return redirect($this->routeTo)->withErrors(['alert-danger' => ConstantMessageService::READ_ERROR]);
        }
    }

    public function update(Request $request, $id)
    {
        $data['pageTitle'] = $this->pageTitle;
        $data['routeTo'] = $this->routeTo;
        try {
            $insertData = $request->except('_token');
            $result = $this->model->where('id', $id)->update($insertData);
            if ($result) {
                return redirect($this->routeTo)->withErrors(['alert-success' => ConstantMessageService::UPDATE]);
            } else {
                return redirect($this->routeTo)->withErrors(['alert-danger' => ConstantMessageService::ADD_FAIL]);
            }
        } catch (\Exception $e) {
            return redirect($this->routeTo)->withErrors(['alert-danger' => ConstantMessageService::ADD_FAIL]);
        }
    }

    public function destroy($id)
    {
        try {
            if (!$this->model->where('id', $id)->count()) {
                return 0;
            }
            if ($this->model->where('id', $id)->where('status', 'completed')->count()) {
                return 0;
            }
            if ($this->orderItem->where('order_id', $id)->count()) {
                return 0;
            }
            $this->model->where('id', $id)->delete();
            $this->orderItem->where('order_id', $id)->delete();
            return 1;
        } catch (Exception $e) {
            return 0;
        }
    }

    public function deleteItem($orderid, $itemid)
    {
        try {
            if (!$this->orderItem->where('order_id', $orderid)->where('item_id', $itemid)->count()) {
                return redirect()->back()->withErrors(['alert-danger' => ConstantMessageService::DELETE_FAIL]);
            }
            $this->orderItem->where('order_id', $orderid)->where('item_id', $itemid)->delete();
            return redirect()->back()->withErrors(['alert-danger' => ConstantMessageService::DELETE]);
        } catch (Exception $e) {
            return $e->getMessage();
            return redirect()->back()->withErrors(['alert-danger' => ConstantMessageService::DELETE_FAIL]);
        }
    }

    public function view($gameid)
    {
        try {
            $data['pageTitle'] = $this->pageTitle;
            $data['routeTo'] = $this->routeTo;
            return view($this->viewUrl . '.gamedetails/view', $data);
        } catch (Exception $e) {
            return redirect($this->redirectUrl)->withErrors(['alert-danger' => ConstantMessageService::READ_ERROR]);
        }

    }


}
