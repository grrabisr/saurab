<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $table = 'settings';

    protected $fillable = [
        'name', 'short_name', 'slogan', 'image', 'mobile', 'phone', 'address', 'email', 'short_description', 'long_description', 'created_at', 'updated_at'
    ];
}
