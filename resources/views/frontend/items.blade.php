@extends('frontend.layouts.app')

@section('content')
    <section class="vizew-post-area mb-50 hero--area section-padding-80 ">
        <div class="container">
            <div class="row">

                <div class="col-12">
                    Order Code: {{$orderDetails->code}}<br>
                    Customer Name: {{$orderDetails->order_by}}<br>
                    Customer Contact: {{$orderDetails->order_by_number}}<br>
                    Table No: @isset($orderDetails->table->name){{$orderDetails->table->name}} @endisset<br><br>
                    <a target="_blank" href="{{URL::to('/details/'.$orderDetails->id)}}" class=" btn btn-lg btn-primary" style="color: #fff;"
                    ><i class="glyphicon glyphicon-eye"></i>Chceck Your Order Details</a>
                    <br>
                    <br>

                    <!-- Single Post Area -->
                    {{--  <div class="single-post-area mb-30 single-widget text-justify text-center ">
                          <div class="row align-items-center text-justify text-center">
                              <div class="col-12 col-lg-6">
                                  <!-- Post Content -->
                                  <div class="post-content mt-0">
                                      <a href="#" class="post-cata cata-sm cata-danger">Links will be updated before 5
                                          minutes of game on</a>
                                      <a href="#" class="post-title mb-2">title</a>


                                      <div class="">
                                          Competition:
                                      </div>
                                  </div>
                              </div>

                          </div>--}}
                <!-- Single Post Area -->

                    @if($category)

                        @foreach($category as $key=>$d)
                            <a href="#" class="post-cata cata-sm cata-danger">{{$key=$key+1}}:{{ucfirst($d->name)}}</a>
                            <table id="" class="table table-bordered thead table-striped">
                                <thead>
                                <tr>
                                    <th>S.N.</th>
                                    <th>Name</th>
                                    <th>Price(Rs.)</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($d->item as $k=>$itemlist)

                                    <tr id="data_{{$d->id}}">
                                        <td>{{$key=$key+1}}</td>
                                        <td>{{ucfirst($itemlist->name)}}</td>
                                        <td>{{$itemlist->price}}</td>

                                        <td>
                                            <a onclick="book('{{$itemlist->id}}');" id="order"
                                               data-id="{{URL::to('/order-item/'.$itemlist->id.'/'.$orderDetails->id)}}"
                                               class="{{ $itemlist->id}} btn btn btn-primary"
                                            ><i class="glyphicon glyphicon-edit"></i> @if(in_array($itemlist->id,$bookedItems))
                                                    Re-order @else Order @endif</a>

                                        </td>
                                    </tr>
                                @endforeach


                                </tfoot>
                            </table>

                        @endforeach


                    @endif


                </div>

            </div>
        </div>
    </section>
@endsection
@section('script')

    <script src="{{asset('almasaeed2010/adminlte/bower_components/datatables.net/js/jquery.dataTables.js')}}"></script>
    <link rel="stylesheet"
          href="{{asset('almasaeed2010/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

    <script src="{{asset('almasaeed2010/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    <script>

        $(document).ready(function () {
            $('#example1').DataTable();
        });

        function book(classname) {
            console.log(classname);
            Swal.fire({
                title: "An input!",
                text: "Write something interesting:",
                input: 'text',
                showCancelButton: true
            }).then((result) => {
                if (result.value) {
                    href = $('.' + classname).attr('data-id')
                    href = href + '/' + result.value
                    console.log(href);
                    console.log("Result: " + result.value);

                    $.ajax({
                        type: "get",
                        url: href,
                        success: function (data, response) {
                            console.log(data);
                            if (data == 1) {
                                swal('Sucessfully Ordered.');
                            }
                            else {
                                swal('Error!! ');

                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            swal('Error!! Could not delete. or category is in use');
                        }

                    });
                }
            });
        }

        $(document).on('click', '#order', function (e) {

        });


    </script>
@endsection
