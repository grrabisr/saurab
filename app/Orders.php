<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Orders extends Authenticatable
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'orders';

    protected $fillable = [
        'code', 'order_by', 'order_by_number','table_id','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        '_token', 'remember_token',
    ];

    public function table()
    {
        return $this->hasOne('App\Tables', 'id', 'table_id');
    }


}
