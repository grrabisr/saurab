<?php

namespace App\Http\Controllers\Admin;


use App\Category;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\TableRequest;
use App\Items;
use App\OrderItem;
use App\Orders;
use App\Tables;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ConstantMessageService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;


class TableController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->pageTitle = "Tables";
        $this->redirectUrl = env('ROUTE_PREFIX_ADMIN') . '/tables/';
        $this->viewUrl = env('ROUTE_PREFIX_ADMIN') . "/tables";
        $this->routeTo = "/" . env('ROUTE_PREFIX_ADMIN') . "/tables/";
        $this->model=new Tables();
        $this->order=new Orders();



    }

    public function index()
    {
        try {
            $data['pageTitle'] = $this->pageTitle;
            $data['routeTo'] = $this->routeTo;

            $data['data'] = $this->model->get();
            return view($this->viewUrl . '.index', $data);
        } catch (\Exception $e) {
            return view($this->viewUrl . '.index', $data)->withErrors(['alert-danger' => ConstantMessageService::READ_ERROR]);
        }
    }

    public function create()
    {
        $data['pageTitle'] = $this->pageTitle;
        $data['routeTo'] = $this->routeTo;
        return view($this->viewUrl . '.create', $data);

    }

    public function store(TableRequest $request)
    {
        $data['pageTitle'] = $this->pageTitle;
        $data['routeTo'] = $this->routeTo;
        try {
            $insertData = $request->except('_token');
            $result = $this->model->create($insertData);
            if ($result) {
                return redirect($this->routeTo)->withErrors(['alert-success' => ConstantMessageService::SUCCESS]);
            } else {
                return redirect($this->routeTo)->withErrors(['alert-danger' => ConstantMessageService::ADD_FAIL]);
            }
        } catch (\Exception $e) {
            return redirect($this->routeTo)->withErrors(['alert-danger' => ConstantMessageService::ADD_FAIL]);
        }
    }

    public function edit($id)
    {
        try {
            $ifExist = $this->model->where('id', $id)->get()->count();
            if (!$ifExist) {
                return redirect($this->routeTo)->withErrors(['alert-danger' => ConstantMessageService::READ_ERROR]);
            }
            $data['pageTitle'] = $this->pageTitle;
            $data['routeTo'] = $this->routeTo;
            $data['data'] = $this->model->where('id', $id)->first();
            return view($this->viewUrl . '.edit', $data);
        } catch (\Exception $e) {
            return redirect($this->routeTo)->withErrors(['alert-danger' => ConstantMessageService::READ_ERROR]);
        }
    }

    public function update(TableRequest $request, $id)
    {
        $data['pageTitle'] = $this->pageTitle;
        $data['routeTo'] = $this->routeTo;
        try {
            $insertData = $request->except('_token');
            $result = $this->model->where('id', $id)->update($insertData);
            if ($result) {
                return redirect($this->routeTo)->withErrors(['alert-success' => ConstantMessageService::UPDATE]);
            } else {
                return redirect($this->routeTo)->withErrors(['alert-danger' => ConstantMessageService::ADD_FAIL]);
            }
        } catch (\Exception $e) {
            return redirect($this->routeTo)->withErrors(['alert-danger' => ConstantMessageService::ADD_FAIL]);
        }
    }

    public function destroy($id)
    {
        try {
            if (!$this->model->where('id', $id)->count()) {
                return 0;
            }
            if ($this->order->where('table_id', $id)->count()) {
                return 0;
            }

            $this->model->where('id', $id)->delete();
            return 1;
        } catch (Exception $e) {
            return 0;
        }
    }




}
