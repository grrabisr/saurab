<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Tables extends Authenticatable
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'tables';

    protected $fillable = [
        'name', 'position', 'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        '_token', 'remember_token',
    ];
}
