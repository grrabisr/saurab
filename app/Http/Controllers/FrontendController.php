<?php

namespace App\Http\Controllers;


use App\Category;
use App\OrderItem;
use App\Orders;
use App\Tables;
use App\Items;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;
use App\Services\ConstantMessageService;

class FrontendController extends Controller
{

    public function __construct()
    {
        $this->redirectUrl = '/';
        $this->viewUrl = "/frontend";
        $this->routeTo = "/";
        $this->table = new Tables();
        $this->order = new Orders();
        $this->category = new Category();
        $this->orderitem = new OrderItem();
        $this->item = new Items ();
        $this->model = new Orders();
        $this->pageTitle = "Orders";



    }

    public function setHomepage()
    {
        $data['routeTo'] = $this->routeTo;
        $data['booked_tables'] = $this->order->where('status', 'processed')->pluck('table_id')->toArray();
        $data['tables'] = $this->table->where('status', true)->orderby('position', 'desc')->get();
        return view('frontend.frontend', $data);
    }


    public function registeruser(Request $request)
    {
        $customer = $request->except('_token');
        $customer['code'] = uniqid();
        $customer['status'] = 'processed';
        if ($customer['table_id'] == '0') {
            $orderDetails = $this->order->where('order_by_number', $customer['order_by_number'])->where('status', 'processed')->with('table:*')->first();
            if (!$orderDetails) {
                return redirect('/');
            }
            $bookeItems = $this->orderitem->where('order_id', $orderDetails->id)->pluck('item_id');
            $data['category'] = $this->category->where('status', true)->orderby('position', 'asc')->with('item:*')->get();
            $data['orderDetails'] = $orderDetails;
            $data['bookedItems'] = $bookeItems->toArray();
            return view('frontend.items', $data);

        }
//        Session::put($customer['order_by_number'], $customer);
//        Session::get($customer['order_by_number']);
        $ifAlreadyBooked = $this->order->where('order_by_number', $customer['order_by_number'])->where('status', 'processed')->get()->count();
        if (!$ifAlreadyBooked) {
            $this->order->create($customer);
        } else {
            if (!$customer['table_id']) {
                return redirect()->back();
            }
        }
        return redirect('/user-items/' . $customer['order_by_number']);

    }

    public function items($phone_number)
    {
        $orderDetails = $this->order->where('order_by_number', $phone_number)->where('status', 'processed')->with('table:*')->first();
        if (!$orderDetails) {
            return redirect('/');
        }
        $bookeItems = $this->orderitem->where('order_id', $orderDetails->id)->pluck('item_id');
        $data['category'] = $this->category->where('status', true)->orderby('position', 'asc')->with('item:*')->get();
        $data['orderDetails'] = $orderDetails;
        $data['bookedItems'] = $bookeItems->toArray();
        return view('frontend.items', $data);

    }

    public function bookitems($itemId, $orderId, $quantity)
    {
        $items = $this->item->find($itemId);
        $insert = array();
        $insert['quantity'] = $quantity;
        $insert['item_id'] = $itemId;
        $insert['price'] = $items->price;
        $insert['order_id'] = $orderId;
        $this->orderitem->create($insert);
        return 1;
    }

    public function details($id)
    {
        try {
            $ifExist = $this->model->where('id', $id)->get()->count();
            if (!$ifExist) {
                return redirect($this->routeTo)->withErrors(['alert-danger' => ConstantMessageService::READ_ERROR]);
            }
            $checkproccessed = $this->model->where('id', $id)->where('status', 'processed')->get()->count();
            if ($checkproccessed) {
                $data['ifprocessed'] = $this->orderitem->where('order_id', $id)->get()->count();
            } else {
                $data['ifprocessed'] = 0;
            }

            $checkCompleted = $this->model->where('id', $id)->where('status', 'completed')->get()->count();
            if ($checkCompleted) {
                $data['ifcompleted'] = 1;
            } else {
                $data['ifcompleted'] = 0;
            }
            $data['items'] = $this->orderitem->where('order_id', $id)->with('item:*')->get();
            $data['pageTitle'] = $this->pageTitle;
            $data['routeTo'] = $this->routeTo;
            $data['data'] = $this->model->where('id', $id)->first();
            $sum=0;
            foreach ($data['items'] as $key => $d) {

                $itemsum=$d->quantity*$d->price;
                $sum=$sum+$itemsum;

            }
            $data['sum']=$sum;
            return view( 'frontend.details', $data);
        } catch (\Exception $e) {
            return $e->getMessage()
;            return redirect($this->routeTo)->withErrors(['alert-danger' => ConstantMessageService::READ_ERROR]);
        }
    }
}
