@extends('admin.layouts.billapp')
@section('header')

    <link rel="stylesheet"
          href="{{asset('almasaeed2010/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

@endsection

@section('content')

        <section class="content">

        @include('errors.errors')
        <!-- Info boxes -->

            <div class="box">
                <div class="box-header">
                    <center>
                        <h3><label>{{$siteinfo->name}}</label></h3><label>{{$siteinfo->email}}</label><br><label>{{$siteinfo->name}}</label><br><label>{{$siteinfo->phone}} {{$siteinfo->mobile}}</label>                    </center>

                    <h3>Invoice</h3>
                    <label>Code: {{$data->code}}</label><br>
                    <label>Name: {{$data->order_by}}</label><br>
                    <label>Contact: {{$data->order_by_number}}</label><br>
                    <label>Date & Time: {{date("Y-m-d H:i:s")}}</label>
                </div>
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered thead-dark table-striped">
                        <thead>
                        <tr>
                            <th>S.N.</th>
                            <th>Item Name</th>
                            <th>Price(Rs)</th>
                            <th>Quantity</th>
                            <th>Total(Rs)</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($items)
                            @foreach($items as $key=>$d)
                                <tr id="data_{{$d->id}}">
                                    <td>{{$key=$key+1}}</td>
                                    <td>{{$d->item->name}}</td>
                                    <td>{{$d->price}}</td>
                                    <td>{{$d->quantity}}</td>
                                    <td>{{$d->quantity*$d->price}}</td>


                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="5" id="grandTotal">Grand Total: {{$sum}} </td>
                            </tr>

                        @else
                            <tr id="data_">
                                <td colspan="3"> No records found.</td>
                            </tr>
                        @endif


                        </tfoot>
                    </table>
                </div>
            </div>


        </section>
        <!-- /.content -->




@endsection

@section('script')

    <script src="{{asset('almasaeed2010/adminlte/bower_components/datatables.net/js/jquery.dataTables.js')}}"></script>
    <link rel="stylesheet"
          href="{{asset('almasaeed2010/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

    <script src="{{asset('almasaeed2010/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    <script>

        $(document).ready(function () {
            window.print();

        });


    </script>
@endsection


