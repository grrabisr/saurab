<?php

namespace App\Http\Controllers\Admin;

use App\Settings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ConstantMessageService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
//use Image; //Intervention Image
use Intervention\Image\Facades\Image as Image;

use Illuminate\Support\Facades\Storage; //Laravel Filesystem

class SettingController extends Controller
{
    public function __construct(Settings $module)
    {
        $this->middleware('auth');
        $this->pageTitle = "Settings";
        $this->redirectUrl = env('ROUTE_PREFIX_ADMIN') . '/settings/';
        $this->viewUrl = env('ROUTE_PREFIX_ADMIN') . "/settings";
        $this->routeTo = "/" . env('ROUTE_PREFIX_ADMIN') . "/settings/";
        $this->model = $module;
        $this->defaultvalues = array();
        $this->defaultvalues['name'] = 'Project Name';
        $this->defaultvalues['short_name'] = 'LF';
        $this->defaultvalues['slogan'] = "This is slogan";
        $this->defaultvalues['image'] = '';
        $this->defaultvalues['mobile'] = '9843169319';
        $this->defaultvalues['phone'] = '';
        $this->defaultvalues['address'] = 'Bhaktapur';
        $this->defaultvalues['email'] = 'info@info.com';
        $this->defaultvalues['short_description'] = 'Your short description';
        $this->defaultvalues['long_description'] = 'Your long description';
    }

    public function index()
    {
        $data['pageTitle'] = $this->pageTitle;
        $data['routeTo'] = $this->routeTo;
        try {

            if ($this->model->get()->count() == false) {
                $data['data'] = $this->defaultvalues;
            } else {
                $data['data'] = $this->model->get()->first()->toArray();
            }
            return view($this->viewUrl . '.index', $data);
        } catch (\Exception $e) {
            $data['data'] = $this->defaultvalues;
            return view($this->viewUrl . '.index', $data)->withErrors(['alert-danger' => $e->getMessage()]);
        }
    }

    public function store(Request $request)
    {
        // print_r($request->except('_token'));        die();
        $data['pageTitle'] = $this->pageTitle;
        $data['routeTo'] = $this->routeTo;
        $insert = $request->except('_token', 'image');
        try {
            if ($this->model->get()->count() == false) {
                if ($request->hasFile('image')) {
                    $file = $request->file('image');
                    //get filename with extension
                    $filenamewithextension = $file->getClientOriginalName();
                    //get filename without extension
                    $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
                    //get file extension
                    $extension = $file->getClientOriginalExtension();
                    //filename to store
                    $filenametostore = md5($filename . microtime()) . '.' . $extension;
                    Storage::put('photos/company/' . $filenametostore, fopen($file, 'r+'));
                    Storage::put('photos/company/thumbnail/' . $filenametostore, fopen($file, 'r+'));
                    //Resize image here
                    $thumbnailpath = public_path('photos/company/thumbnail/' . $filenametostore);
                    $img = Image::make($thumbnailpath)->resize(100, 100, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $img->save($thumbnailpath);
                    $insert['image'] = $filenametostore;
                }
                $this->model->create($insert);
            } else {
                $insert['image'] = $this->model->get()->first()->toArray()['image'];

                if ($request->hasFile('image')) {
                    $file = $request->file('image');
                    //get filename with extension
                    $filenamewithextension = $file->getClientOriginalName();
                    //get filename without extension
                    $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
                    //get file extension
                    $extension = $file->getClientOriginalExtension();
                    //filename to store
                    $filenametostore = md5($filename . microtime()) . '.' . $extension;
                    Storage::put('photos/company/' . $filenametostore, fopen($file, 'r+'));
                    Storage::put('photos/company/thumbnail/' . $filenametostore, fopen($file, 'r+'));
                    //Resize image here
                    $thumbnailpath = public_path('photos/company/thumbnail/' . $filenametostore);
                    $img = Image::make($thumbnailpath)->resize(100, 100, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $img->save($thumbnailpath);
                    $insert['image'] = $filenametostore;
                }
                $this->model->truncate();
                $this->model->create($insert);
            }

            return redirect($this->redirectUrl)->withErrors(['alert-success' => ConstantMessageService::SUCCESS]);

        } catch (\Exception $e) {

            if ($this->model->get()->count() == false) {
                $data['data'] = $this->defaultvalues;
            } else {
                $data['data'] = $this->model->get()->first()->toArray();
            }
            return view($this->viewUrl . '.index', $data)->withErrors(['alert-danger' => $e->getMessage()]);
        }

    }


}
