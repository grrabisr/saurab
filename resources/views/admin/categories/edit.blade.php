@extends('admin.layouts.app')
@section('header')

    <link rel="stylesheet"
          href="{{asset('almasaeed2010/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="{{URL::to('/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active"><a href="{{URL::to($routeTo)}}">{{$pageTitle}}</a></li>
            </ol>
            <h2 class="box-title">
                Edit {{$pageTitle}}
            </h2>
        </section>

        <!-- Main content -->


        <section class="content">

        @include('errors.errors')
        <!-- Info boxes -->
            <div class="box">
                <div class="box-header">

                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <div class="box ">
                        <div class="box-header with-border">
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form class="form-horizontal" action="{{URL::to($routeTo).'/update/'.$data->id}}" method="post">
                            <input name="_token" type="hidden" value="{{csrf_token()}}">
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Name</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" value="{{$data->name}}" name="name" placeholder="Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Position</label>

                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" value="{{$data->position}}" name="position"
                                               placeholder="Position">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Status</label>
                                    <div class=" col-sm-10">
                                        <div class="radio">
                                            <label><input type="radio" value="1" name="status"  @if($data->status) checked @endif >Active</label>
                                        </div>
                                        <div class="radio">
                                            <label><input value="0" type="radio" name="status"  @if(!$data->status) checked @endif>Inactive</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <a href="{{URL::to($routeTo)}}" class="btn btn-default">Cancel</a>
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>

        </section>
        <!-- /.content -->
    </div>




@endsection

@section('script')


@endsection
