@extends('frontend.layouts.app')

@section('content')
    <section class="vizew-post-area mb-50 hero--area section-padding-80">
        <div class="container">
            <div class="row">
                <div class="m-auto p-2 text-justify text-center col-12 col-sm-12 col-md-8 col-lg-8 ">
                    <div class="all-posts-area">
                        <!-- Single Post Area -->

                        <!-- Single Post Area -->

                        <div class="single-post-area mb-30 single-widget">
                            <div class="row align-items-center">


                                <div class="col-12 col-lg-12">
                                    Note:Please register and reserve table before ordering items. Please inform the
                                    counter if you need to cancel reserved table after registeration.
                                    <form action="{{route('registeruser')}}" method="POST" class="col-md-12">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input name="_token" type="hidden" value="{{csrf_token()}}">

                                        <input required type="text" name="order_by" class="form-control"
                                               placeholder="Your Full Name">
                                        <input type="number" name="order_by_number" class="form-control"
                                               placeholder="Your Number" required>
                                        <select required name="table_id" class="form-control">
                                            <option value=''>Select Table</option>

                                            @if($tables)
                                                @foreach($tables as $key=>$d)
                                                    <option @if(strtolower($d->name)!=='previous table') @if(in_array($d->id,$booked_tables))
                                                     disabled @endif  value="{{$d->id}}" @else value='0' @endif>{{ucfirst($d->name)}}</option>
                                                @endforeach
                                            @endif

                                        </select>
                                        <button type="submit" class="btn btn-info">Register
                                        </button>
                                    </form><!-- Post Content -->

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
