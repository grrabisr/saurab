<?php

namespace App\Services;

class ConstantMessageService
{
    const SUCCESS = 'Data added successfully.';
    const ADD_FAIL = 'Data failed to add!';
    const UPDATE = 'Data updated successfully.';
    const SAVED = 'Data saved successfully.';
    const UPDATE_FAIL = 'Data failed to udpate!';
    const DELETE = 'Data delete successfully.';
    const DELETE_FAIL = 'Data failed to delete!';
    const DELETE_IMG = 'Image delete successfully.';
    const UPDATE_PASSWORD = 'Password update successfully.';
    const READ_ERROR = 'Data read error.';
    const DELETE_ERROR_ON_EXIST = 'Data in use.';
    const NO_RECORD_FOUND = 'Records not found.';
    const RECORD_FOUND = 'Records found.';
    const EMAIL_FAILED = 'Data added successfully. Some emails could not receive the mail.';
}