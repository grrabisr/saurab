@extends('admin.layouts.app')
@section('header')

    <link rel="stylesheet"
          href="{{asset('almasaeed2010/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">

            <ol class="breadcrumb">
                <li><a href="{{URL::to('/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active"><a href="{{URL::to($routeTo)}}">{{$pageTitle}}</a></li>
            </ol>
            <h2 class="box-title">
                {{$pageTitle}}
            </h2>
        </section>

        <!-- Main content -->


        <section class="content">

        @include('errors.errors')
        <!-- Info boxes -->
            <div class="box">
                <div class="box-header">
                    <a href="{{URL::to($routeTo).'/create'}}" class="btn btn-sm btn-success"
                    ><i class="glyphicon glyphicon-eue"></i>Add New</a>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered thead-dark table-striped">
                        <thead>
                        <tr>
                            <th>S.N.</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Price(Rs.)</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($data)
                            @foreach($data as $key=>$d)
                                <tr id="data_{{$d->id}}">
                                    <td>{{$key=$key+1}}</td>
                                    <td>{{ucfirst($d->name)}}</td>
                                    <td>{{$d->category->name}}</td>
                                    <td>{{$d->price}}</td>
                                    <td>@if($d->status) Active @else Inactive @endif</td>

                                    <td>
                                        <a href="" id="deleteRecords" class="btn btn-sm btn-danger"
                                           data-id="{{$d->id}}"><i class="glyphicon glyphicon-trash"></i>Delete</a>
                                        <a href="{{URL::to($routeTo).'/edit/'.$d->id}}" class="btn btn-sm btn-primary"
                                        ><i class="glyphicon glyphicon-edit"></i>Edit</a>

                                    </td>
                                </tr>
                            @endforeach

                        @else
                            <tr id="data_">
                                <td colspan="3"> No records found.</td>
                            </tr>
                        @endif


                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.row -->


            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>




@endsection

@section('script')

    <script src="{{asset('almasaeed2010/adminlte/bower_components/datatables.net/js/jquery.dataTables.js')}}"></script>
    <link rel="stylesheet"
          href="{{asset('almasaeed2010/adminlte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

    <script src="{{asset('almasaeed2010/adminlte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    <script>

        $(document).ready(function () {
            $('#example1').DataTable();
        });

        $(document).on('click', '#deleteRecords', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "get",
                        url: "{{URL::to($routeTo).'/'}}" + id,
                        success: function (data, response) {
                            $('#data_' + id).hide();
                            console.log(response);
                            if (response) {
                                swal('Sucessully Deleted.');
                            }
                            else {
                                swal('Error!! Could not delete.');

                            }

                        }

                    });
                }
            })


        });

    </script>
@endsection
